# Utilise l'image de base Odoo 17
FROM odoo:17

# Copie le fichier de configuration personnalisé dans le conteneur
COPY odoo.conf /etc/odoo/odoo.conf

# Expose le port 8069 (port par défaut pour Odoo)
EXPOSE 8069
